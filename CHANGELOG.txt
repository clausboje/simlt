2022-12-30
    fix position store of image, when moved on lightt table

2022-11-28
    Change project file (ltp file) to have relative path to the ltp file.
    Store path as generic (std::filesystem::path), posibility to copy
	 project from between OS (windows/Linux  OSX ???) 

	 Icon for the program, thanks to Carsten Popp

2022-11-05
    Save/Save As working

2022-10-30
    Dialog position when copy/mode corrected
    Do not steal focus when mouse over windows

2022-10-25
    Mouse wheel working
    PageUP/Down home/end arrows working
    Only visible part of the LigthTable is drawn, speed program up.
    
2022-10-22
    Filename under image.
    Image in alphabetical order.
    Corrected some misspelling, mostly because I am danish, feel free to continue
    Lots of small errors .... an ongoing process ;-)
