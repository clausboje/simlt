#ifndef __project_H
#define __project_H

#include <string>
#include <list>
#include <iostream>
#include <wx/bitmap.h>
#include "image.h"


class image_C;
class project_C;

class LightTableImage_C
{
   public:
        unsigned int ImageID;           // Image id
        std::string FileName;           // Used for to speed up Export / save result and filename
        wxBitmap* thumpnail;            // Easy acces to thumnail for this image
        wxBitmap* unselected_thumpnail; // Easy acces to thumnail for this image
        wxBitmap* selected_thumpnail;   // Easy acces to thumnail for this image
        unsigned int posX;              // Position on LightTable
        unsigned int posY;              // Position on LightTable
        unsigned int posX_RB;           // Position on LightTable right bottom
        unsigned int posY_RB;           // Position on LightTable right bottom
        unsigned int posY_forText;      // Position for text relative to thumbnail 0,0
        void SetSelect(bool i_select);  // Set select or deselect
        bool GetSelect();               // Get select or deselect

   private:
        bool selected;                  // Image is selected
};

class LightTable_C
{
   public:
      LightTable_C(project_C* parrent, unsigned int i_ID);
      ~LightTable_C();
      project_C* pProject;                                              // Pointer to project class 
      unsigned int getLightTable_ID() { return LightTable_ID; };   // yyy Skal over i cpp file
      std::list<LightTableImage_C*>  LightTableImages;
      LightTableImage_C* InsertImage(unsigned int ImageID, int posX = -1, int posY = -1, image_C* im=NULL);
      void DeleteImage(unsigned int ImageID);
      void UpdateImagePosition(unsigned int imageID, unsigned int posX, unsigned int posY);
      int FindSelected();                                               // Return id of iselected image -1: nothing done
      wxBitmap* Select(unsigned int imageID, bool i_select);            // return pointer to image
      void SelectAll(bool i_select);                                    // Select/deselct All
      void GetFreePos(int& posX, int& posY);                            // Find free pos on LightTable
      //yyybool UpdatePaintImage();

      void SaveLightTableOutput(std::filesystem::path i_path);          // Store data in file with image sorte as place on the LightTable 

   private:
      unsigned int LightTable_ID;
      unsigned int debugCNT;
      bool UpdatePI;                                                    // update paint image
};

class project_C
{
   public:
      project_C();
      ~project_C();
      
      unsigned int FrameSizeX;                              // size of LightTables (all same size !!!) 
      unsigned int FrameSizeY;                              // size of LightTables (all same size !!!)s
      std::list<image_C*>  images;
      std::list<LightTable_C*>  LightTables;

      bool NewProject(std::string path);
      bool SaveProject(std::string i_path);
      void ExportAll(std::string i_path);
      bool OpenProject(std::string i_path);
      LightTable_C* AddLightTable(unsigned int i_ID);
      unsigned int GetNumberOfLightTables();                                   // Return number of LightTables
      std::string &GetProjectFilePath();                                       // Return number of LightTables

      image_C* FindImage(unsigned int ImageID);                                // Find image from ImageID
      int CopyToLightTable(unsigned int ImageID, int ToLightTableID = -1);     // Copy selected image from imageID to LightTableID
      
      bool ProjectNotSaved();                                                   // Return project saved status
      void Set_NotSaved();                                                      // Set project saved status to save
      

    public:
      bool project_loaded;

    private:
        std::string ProjectName;                // Project name
        std::string ProjectFilePath;            // Path to project file
        unsigned int imageThumpnailWidth;       // Width of thumnail showned on LightTable
        unsigned int NumberOfHorisontal;        // Number of horisontal images
        unsigned int PixelBetween;              // Numer of pixels between images
        unsigned int NumberOfVerticalFree;      // Number of free space in collums on LightTable.
        unsigned int NumberOfHorisontalFree;    // Number of free space in rows on LightTable.

        unsigned int image_ID;                  // Store next ID for images
        unsigned int maxVertical;               // Store vertical maxsize of thumnail
        bool ProjectNotSaved_store;             // If true, ask for save project 
        



};

#endif
