#ifndef __image_H
#define __image_H

#include <wx/image.h>
#include <wx/bitmap.h>
#include <string>
#include <iostream>
#include <filesystem>
#include "project.h"

//class project_C;

class image_C
{
   public:
      std::filesystem::path filepath;
      wxBitmap* us_image;                 // unselected image
      wxBitmap* s_image;                  // Selected image
      unsigned int width;
      unsigned int height;
      unsigned int width_T;               //thumpnail width
      unsigned int height_T;              //thumpnail height
      unsigned int height_T_forText;      //thumpnail height
      bool isOK();                        // return ok if image is OK
      void MakeFilenameImg(wxImage& im, bool select);

   public:
      image_C(std::filesystem::path i_filepath, unsigned int i_width_T, unsigned int i_imageID);
      ~image_C();
      unsigned int GetImageID() {return imageID; }
      std::filesystem::path filepath_rel(std::filesystem::path rel2path);

      
      
   private:
      unsigned int imageID;               // ID of image, just a number
      bool loaded_ok;
};

#endif
