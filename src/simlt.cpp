/////////////////////////////////////////////////////////////////////////////
// Project:     simlt
// Author:      Claus Boje
// Copyright:   (C) 2022 Claus Boje
// Copyright    (C) 2022 Simple Light Table Developers, see AUTHORS.txt for contributors.
//
// Licence:     GPLv3
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
//
// Todo
//   Move marked images together
//   top images flikers when scrolling !!!!
//   some mark shoing not saved !!!
//   Setup system
//   Lave call another program nomacs / gimp / 
//   Lave Delete LT
//      lave advarselsdialog
//   CLI interface           
//   Resize thumpnail
//   Posibillity to set the size of thumpnails.
//   Lave en Delete image
//   Bedre select
//   Mouse wheel funktion
//   plugin system
//
//   Posibillity to have the same image more than ones in a LT
//          maybe copy to same LT
//   Posibillity to import images in existing project 
//         This is prepared but setup missing.
//   Posibillity to translate to different languages
//
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#include "wx/scrolbar.h"
#include "wx/scrolwin.h"
#include "wx/filedlg.h"
#include "wx/dirdlg.h"
#include "wx/kbdstate.h"
//#include <wx/textctrl.h>
//#include <wx/txtstrm.h>

#endif

#include "wx/image.h"

// Under Windows, change this to 1
// to use wxGenericDragImage

#define wxUSE_GENERIC_DRAGIMAGE 1

#if wxUSE_GENERIC_DRAGIMAGE
#include "wx/generic/dragimgg.h"
#define wxDragImage wxGenericDragImage
#else
#include "wx/dragimag.h"
#endif


#ifndef wxHAS_IMAGES_IN_RESOURCES
    #include "pgicon.xpm"
    #include "dragicon.xpm"
#endif
#include <iostream>
#include <string>         // std::string
#include <cstddef>         // std::size_t
#include <list>
#include "image.h"
#include "project.h"
#include "simlt.h"

// IDs for the controls and the menu commands
enum
{
    // menu items
    Event_Quit = 1,
    Event_LT,
    //Event_About,
    //Event_Bind,
    //Event_Connect,
    //Event_Dynamic,
    //Event_Push,
    //Event_Pop,
    //Event_Custom,
    //Event_Test,
    Event_copy,
    Event_move,
    Event_new,
    Event_export,
    //Event_save
    //Event_load
};
//https://docs.wxwidgets.org/3.0/overview_events.html


// LTFrame_C
//  EVT_CLOSE                     (wxID_CLOSE, LTFrame_C::OnClose)
IMPLEMENT_DYNAMIC_CLASS( LTFrame_C, wxFrame )

wxBEGIN_EVENT_TABLE(LTFrame_C,wxFrame)
  EVT_MENU                      (wxID_ABOUT, LTFrame_C::OnAbout)
  EVT_MENU                      (wxID_EXIT,  LTFrame_C::OnQuit)
  EVT_CLOSE                     (LTFrame_C::OnClose)
  EVT_MENU                      (Event_LT, LTFrame_C::event_Create_LT)
  //EVT_MENU              (Event_copy, LTFrame_C::event_Create_LT)
  EVT_AUI_PANE_CLOSE            (LTFrame_C::OnPaneClose)
  EVT_MENU                      (Event_new, LTFrame_C::NewLightTable)
  EVT_MENU                      (Event_export, LTFrame_C::ExportAll)
  EVT_MENU                      (wxID_SAVE, LTFrame_C::FileSave)
  EVT_MENU                      (wxID_SAVEAS, LTFrame_C::FileSaveAs)
  EVT_MENU                      (wxID_OPEN, LTFrame_C::ProjectOpen)
wxEND_EVENT_TABLE()
  //EVT_CHAR_HOOK                 (LTFrame_C::OnKeyEvent)

LTFrame_C::LTFrame_C()
: wxFrame( (wxFrame *)NULL, wxID_ANY, wxT("Light Table"),
          wxDefaultPosition, wxSize(1000,700) )
// =======================================================================================
// LTFrame
// Main window for all canvases
// ---------------------------------------------------------------------------------------
// Coded by: CB 
// =======================================================================================
{
    ImageID = 0;                                                        // Init ImageID
    wxMenu *file_menu = new wxMenu();

    file_menu->Append(Event_new, wxT("&New\tCtrl-N"), wxT("New Project"));
    file_menu->Append(wxID_SAVE, wxT("&Save\tCtrl-S"), wxT("Save Project"));
    file_menu->Append(wxID_SAVEAS, wxT("Save &As\tCtrl-Shift-S"), wxT("Save Project As"));
    file_menu->Append(wxID_OPEN, wxT("&Open\tCtrl-O"), wxT("Open Project"));
    file_menu->Append(Event_LT, wxT("&New LT\tCtrl-L"), wxT("Make new LT"));
    file_menu->Append( wxID_EXIT, wxT("E&xit"));
        
    wxMenu *export_menu = new wxMenu();
    export_menu->Append( Event_export, wxT("&Export all"));

    wxMenu *about_menu = new wxMenu();
    about_menu->Append( wxID_ABOUT, wxT("&About"));

    wxMenuBar *menu_bar = new wxMenuBar();
    menu_bar->Append(file_menu, wxT("&File"));
    menu_bar->Append(export_menu, wxT("&Export LT"));
    menu_bar->Append(about_menu, wxT("&About"));

    SetIcon(wxICON(pgicon));
    SetMenuBar( menu_bar );

#if wxUSE_STATUSBAR  // Skal undersoeges
    CreateStatusBar(2);
    int widths[] = { -1, 100 };
    SetStatusWidths( 2, widths );
#endif // wxUSE_STATUSBAR
    m_mgr.SetManagedWindow(this);           // Start AUI
    m_mgr.SetFlags(wxAUI_MGR_DEFAULT | wxAUI_MGR_ALLOW_ACTIVE_PANE);
    m_mgr.SetDockSizeConstraint(0.5,0.5);

    //Create_LTcanvas();                      // make canvas
}

LTFrame_C::~LTFrame_C()
// =======================================================================================
// LTFrame
// Main window for all canvases
// ---------------------------------------------------------------------------------------
// Coded by: CB 
// =======================================================================================
{
    for (LTLightTable_C* LT_display: LTs)              // Run trough list for display
    {
       delete LT_display;
    }
    m_mgr.UnInit();
}

void LTFrame_C::OnClose(wxCloseEvent& event)
// =======================================================================================
// LTFrame
// Closedown window for all canvases
// =======================================================================================
{
    if ( event.CanVeto() && project.ProjectNotSaved())
    {
        if ( wxMessageBox("The project has not been saved... continue closing?",
                          "Please confirm",
                          wxICON_QUESTION | wxYES_NO | wxNO_DEFAULT) != wxYES )
        {
            event.Veto();
            return;
        }
    }
    event.Skip();
}

void LTFrame_C::NewLightTable(wxCommandEvent& WXUNUSED(event) )
{
    if (!project.project_loaded)
    {
        // Open dialog for selecting of directory.
        wxDirDialog dialog(this,
                           wxT("Directory find"),
                           wxEmptyString);
    
    
        if (dialog.ShowModal() == wxID_OK)
        {
            //std::cout << "DEBUD directory" << dialog.GetPath().c_str() << "\n"; // Debug
           std::string ft = (std::string) dialog.GetPath().c_str();        // kun mellemvariabel ft for debug
           if (project.NewProject(ft))                                     // Images found and loaded
           {
               Create_LTcanvas();                                          // make canvas
           }
           else
           {
               wxLogMessage(wxT("No images found !!!"));
           }
        }
    }
    else
    {
        wxLogMessage(wxT("Project already loaded !!!"));
    }
}

void LTFrame_C::FileSave(wxCommandEvent& WXUNUSED(event) )
{
    if (project.GetProjectFilePath().empty())
    {
        wxCommandEvent evl = wxID_SAVEAS;
        FileSaveAs(evl);
    }
    else
    {
       project.SaveProject(project.GetProjectFilePath()); 
        
    }
}

void LTFrame_C::FileSaveAs(wxCommandEvent& WXUNUSED(event) )
{
    wxString fileName;
    if (project.GetProjectFilePath().empty())
    {
        fileName = wxT("LightTable.ltp");
    }
    else
    {
        fileName = (wxString) project.GetProjectFilePath();
    }
   
    wxFileDialog dialog(this,
                        wxT("Save project file as"),
                        wxEmptyString,
                        fileName,
                        wxT("Project file files (*.ltp)|*.ltp|All files|*"),
                        wxFD_SAVE|wxFD_OVERWRITE_PROMPT);

    //dialog.SetFilterIndex(1);

    if (dialog.ShowModal() == wxID_OK)
    {
        project.SaveProject((std::string) dialog.GetPath()); 
    }
}

void LTFrame_C::ProjectOpen(wxCommandEvent& WXUNUSED(event) )
{
    if (!project.project_loaded)
    {
       wxFileDialog dialog(this,
                           wxT("Open project file"),
                           wxEmptyString,
                           wxEmptyString,
                           wxT("Project file files (*.ltp)|*.ltp|All files|*"),
                           wxFD_OPEN|wxFD_FILE_MUST_EXIST);
 
       //dialog.SetFilterIndex(1);
 
       if (dialog.ShowModal() == wxID_OK)
       {
           project.OpenProject((std::string) dialog.GetPath());
       }
       for (LightTable_C* LT: project.LightTables)
       {
            Create_LTcanvas(LT->getLightTable_ID());
       }

    }
    else
    {
        wxLogMessage(wxT("Project already loaded !!!"));
    }
}

void LTFrame_C::ExportAll(wxCommandEvent& WXUNUSED(event) )
// =======================================================================================
// Store result of all Light Tables in files
// =======================================================================================
{
    wxFileDialog dialog(this,
                        wxT("Save Light Tabels"),
                        wxEmptyString,
                        wxEmptyString,
                        wxT("Light Tabels (*.txt)|*.txt|All files|*"),
                        wxFD_SAVE|wxFD_OVERWRITE_PROMPT);

    //dialog.SetFilterIndex(1);

    if (dialog.ShowModal() == wxID_OK)
    {
        project.ExportAll((std::string) dialog.GetPath());
    }
}

void LTFrame_C::Create_LTcanvas(int i_ID)
// =======================================================================================
// Create a new LightTable
// =======================================================================================
{
        int ID;
        // Calculate id for canvas
        if (i_ID == -1)
        {
            ID = LT_id;
        }
        else
        {
            ID = i_ID;
        }

        LTLightTable_C* LT = new LTLightTable_C(ID, 
                                        wxSize(project.FrameSizeX,project.FrameSizeY),
                                        this, wxID_ANY, 
                                        wxPoint(0,0),
                                        wxSize(1000,800),
                                        (wxHSCROLL|wxVSCROLL));

        LTs.push_back(LT);                             // Insert in the end of the list
        LT->pLTables = project.AddLightTable(ID);      // Add Light Table in project

        

        for (LTLightTable_C* LT_display: LTs)          // Run trough list (LTs) of canvases for display
        {
            if (!LT_display->shown)
            {
                wxString PaneName = "LT " + std::to_string(LT_display->LTcanvas_ID + 1);
                m_mgr.AddPane(LT_display, wxAuiPaneInfo().
                           Name(PaneName).Caption(PaneName).MaximizeButton().
                           Row(LT_display->LTcanvas_ID));
                LT_display->SetVirtualSize(wxSize(project.FrameSizeX,project.FrameSizeY));          // !!! This must be set to get the scrollbars to work  https://stackoverflow.com/questions/12496427/wxscrolledwindow-scrollbar-not-working
                LT_display->SetScrollRate(1, 1);                                                    // !!! This must be set to get the scrollbars to work  https://stackoverflow.com/questions/12496427/wxscrolledwindow-scrollbar-not-working

                LT_display->shown = true;
 
                // Show images from project
                if (!LT->pLTables->LightTableImages.empty())                                             // Any images for this canvas ?
                {
                    for (LightTableImage_C* shapeImage: LT->pLTables->LightTableImages)                      // Run trough LightTableImages_C in LightTable_C to setup shapes to display
                    {
                        //DragShape* newShape = new DragShape(&shapeImage->thumpnail, shapeImage->ImageID);
                        DragShape* newShape = new DragShape(shapeImage);
                        newShape->SetPosition(wxPoint(shapeImage->posX,shapeImage->posY));
                        newShape->SetDragMethod(SHAPE_DRAG_BITMAP);
                        LT_display->GetDisplayList().push_back(newShape);                            // Create list of shapes in LTLightTable_C
                    }
                }
            }
        }

        m_mgr.Update();
        
        if (ID >= LT_id)
        {
            LT_id = ID + 1;        // set next id
        }
}

void LTFrame_C::OnQuit( wxCommandEvent &WXUNUSED(event) )
{
    Close( true );
}

void LTFrame_C::OnAbout( wxCommandEvent &WXUNUSED(event) )
{
    (void)wxMessageBox( wxT("Simple Light Table\n")
        wxT("Release 0.01 20221025\nClaus Boje (c) 2022"),
        wxT("About Simple Light Table"),
        wxICON_INFORMATION | wxOK );
}

void LTFrame_C::event_Create_LT(wxCommandEvent& WXUNUSED(event))
// =======================================================================================
// Treat event for creating a new LightTable
// ---------------------------------------------------------------------------------------
// Coded by: CB 
// =======================================================================================
{
    if (project.project_loaded)
    {
        Create_LTcanvas();                                                  // make canvas
    }
    else
    {
        wxLogMessage(wxT("Cannot create new LT, no project loaded !!!"));
    }
}
/*
void LTFrame_C::OnKeyEvent(wxKeyEvent& event)
{
   if ( event.GetModifiers() == wxMOD_SHIFT )
   {
      std::cout << "SHIFT er trykket LTframe" <<  std::endl;
   }
      std::cout << "noget er trykket LTframe" <<  event.GetModifiers()<< std::endl;
   event.Skip();
}*/


void LTFrame_C::OnPaneClose(wxAuiManagerEvent& evt)
// =======================================================================================
// Close a LightTable
// ---------------------------------------------------------------------------------------
// Coded by: CB 
// =======================================================================================
{
    std::cout << "DEBUG event close" << evt.pane->name <<"\n";
}

// LTLightTable_C

IMPLEMENT_CLASS(LTLightTable_C, wxScrolledWindow)

wxBEGIN_EVENT_TABLE(LTLightTable_C, wxScrolledWindow)
  EVT_PAINT             (LTLightTable_C::OnPaint)
  EVT_ERASE_BACKGROUND  (LTLightTable_C::OnEraseBackground)
  EVT_KEY_DOWN          (LTLightTable_C::OnKeyEventDown)
  EVT_KEY_UP            (LTLightTable_C::OnKeyEventUp)
  EVT_MOUSE_EVENTS      (LTLightTable_C::OnMouseEvent)
  EVT_MENU              (Event_copy, LTLightTable_C::copy_event)
  EVT_MENU              (Event_move, LTLightTable_C::move_event)
wxEND_EVENT_TABLE()

LTLightTable_C::LTLightTable_C(unsigned int LT_id, wxSize i_size, wxWindow *parent, wxWindowID id,
                       const wxPoint &pos, const wxSize &size, long style )
                       : wxScrolledWindow( parent, id, pos, size, style )
// =======================================================================================
// LTcanvas - The LightTable
// ---------------------------------------------------------------------------------------
// Coded by: CB 
// =======================================================================================
{
    pLTFrame = (LTFrame_C*) parent;
    sizeCanvas = i_size;
    SetBackgroundColour(* wxWHITE);
    std::list<LTLightTable_C*> LTs;                         // list of canvases
    default_copy_move_item = 0;

    SetCursor(wxCursor(wxCURSOR_ARROW));

    m_dragMode = TEST_DRAG_NONE;
    m_draggedShape = (DragShape*) NULL;
    m_dragImage = (wxDragImage*) NULL;
    m_currentlyHighlighted = (DragShape*) NULL;

    Keyboard_Modifiers  = 0;

    shown = false;
    LTcanvas_ID = LT_id;

    debugCNT = (LT_id+1) * 10000;
}

LTLightTable_C::~LTLightTable_C()
// =======================================================================================
// LTcanvas - Destructor
// =======================================================================================
{
    ClearShapes();

    if (m_dragImage)
        delete m_dragImage;
}

void LTLightTable_C::copy_event( wxCommandEvent& WXUNUSED(event) )
// =======================================================================================
// LTcanvas - event copy to other canvas
// =======================================================================================
{
  std::cout << "copy_event" << std::endl;
  DoCopy();
}

int LTLightTable_C::DoCopy()
// =======================================================================================
// LTcanvas - copy to other canvas
// Return ToCanvasID  -1 no copy
// =======================================================================================
{
  int ToCanvasID = -1;
  std::cout << "DEBUG DoCopy to other canvas My id: " << LTcanvas_ID << std::endl;
  // If only 1 canvas make an error dialog.
  // If only 2 Lighttabels (canvases) take the other one
  // More than 2 Canvases use CopyMove dialog
  switch (pLTables->pProject->GetNumberOfLightTables())
  {
          case 0:  std::cout << "size 0" << std::endl;
                   wxLogMessage(wxT("No Lighttable !!!"));
                   return ToCanvasID;
                   break;

          case 1:  std::cout << "size 1" << std::endl;
                   wxLogMessage(wxT("Only one Lighttable"));
                   return ToCanvasID;
                   break;
          case 2:  std::cout << "size 2" << std::endl;
                   ToCanvasID = pLTables->pProject->CopyToLightTable(LTcanvas_ID);
                   if (ToCanvasID < 0)              // No ToCanvas
                   {
                        wxLogMessage(wxT("No canvas to copy to...."));
                        return ToCanvasID;
                   }
                   break;
          default:
                   // Dialog box to get tocanvas
                   CopyMove_C dialog(pLTFrame,default_copy_move_item,NULL, wxID_ANY, wxEmptyString);
                   dialog.ShowModal();
                   ToCanvasID = dialog.getCanvasID();

                   if (ToCanvasID < 0)              // No ToCanvas
                   {
                        wxLogMessage(wxT("Canvas error !!!"));
                        return ToCanvasID;
                   }
                   if (ToCanvasID == (int) LTcanvas_ID)
                   {
                        wxLogMessage(wxT("Duplicate image(s), not implemented yet"));
                        return -1;
                   }
                   else
                   {
                       ToCanvasID = pLTables->pProject->CopyToLightTable(LTcanvas_ID, ToCanvasID);
                       if (ToCanvasID < 0)              // No ToCanvas
                       {
                          wxLogMessage(wxT("No canvas to copy to...."));
                          return -1;
                       }
                   }
                   break;
  }
  // Call ToCanvas for update
  for (LTLightTable_C* ToCanvas: pLTFrame->LTs)                          // Find pointer to ToCanvas
  {
      if (ToCanvas->LTcanvas_ID == (unsigned int) ToCanvasID)
      {
            ToCanvas->DoUpdate();
            break;
      }
  }
  return ToCanvasID;
}


void LTLightTable_C::move_event( wxCommandEvent& WXUNUSED(event) )
// =======================================================================================
// LTcanvas - event move to other canvas
// =======================================================================================
{
  DoMove();
}

void LTLightTable_C::DoMove()
// =======================================================================================
// LTcanvas - move to other canvas
// =======================================================================================
{
   // Copy all selected.
   if (DoCopy() >= 0)
   {
      // Remove all selected.
      DragShape* ptr;
      int IDtoDelete = pLTables->FindSelected();
      while (IDtoDelete >= 0)
      {
         for (std::list<DragShape*>::iterator it = m_displayList.begin(); it != m_displayList.end();)
         {
              ptr=*it;
              std::cout  << " indhold " << ptr->GetID() << std::endl;
              if (ptr->GetID()== (unsigned int) IDtoDelete)
              {
                  delete ptr;                                     // remove DragShape for thsi item
                  it = m_displayList.erase(it);                   // remove from list of images for display
                  pLTables->DeleteImage(IDtoDelete);               // Delete form pLTables
                  break;                                          // ID found no reason ton continue
              }
              else
              {
                  it++;
              }
         }
         IDtoDelete = pLTables->FindSelected();                    // find next or stop
      }
      DoUpdate();
   }
}


void LTLightTable_C::DoScroll(int step, bool autostep, bool vertical)
// =======================================================================================
// LTLightTable - Scroll
// Step (signed step scroll)
// autostep: false: use step new step
//           true: calculate step from vissible size
//           Step -1: step up/left
//           Step +1: step down/right
//           Step <-1: home
//           step > 1: end
// =======================================================================================
{
    // Get position and size of visible window
    int x_tl;                 // x top left
    int y_tl;                 // y top left
    GetViewStart(&x_tl,&y_tl);
    int x_size;
    int y_size;
    GetClientSize(&x_size,&y_size);

    int x_set = x_tl;
    int y_set = -1;
    std::cout << " x_tl: " << x_tl << " y_tl: " << y_tl << " x_size: " << x_size << " y_size: " << y_size << std::endl;

    //DEBUG 
    int xu,yu;
    GetScrollPixelsPerUnit(&xu,&yu);
    std::cout << "xu,yu: " << xu << "," << yu << std::endl;

    // Calculate new position
    if (vertical)                // Right/left shift
    {
        y_set = y_tl;
        x_set = x_tl + step;
        if (x_set < 0)
        {
           x_set = -1;
        }
        else if (x_set >= (int) pLTables->pProject->FrameSizeX)
        {
           x_set = -1;
        }
    }
    else                         // Up/down scroll
    {
        if (autostep)
        {
          if (step < -1)               // home
          {
              x_set = 0;
              y_set = 0;
          }
          else if (step > 1)           // end
          {
              x_set = pLTables->pProject->FrameSizeX - x_size;
              y_set = pLTables->pProject->FrameSizeY - y_size;
          }
          else if (step == -1)         // PageUp
          {
              y_set = y_tl - (y_size/2) ;
              if (y_set < 0)
              {
                 y_set = 0;
              }
          }
          else if (step == 1)         // PageDown
          {
              y_set = y_tl + (y_size/2) ;
              std::cout << y_set << "FrameSizeY: " << pLTables->pProject->FrameSizeY <<std::endl;
              if (y_set > ((int) pLTables->pProject->FrameSizeY - y_size))
              {
                 y_set = pLTables->pProject->FrameSizeY - y_size;
              }
              std::cout << y_set << std::endl;
          }
        }
        else
        {
           y_set = y_tl + step;
           if (y_set < 0)
           {
              y_set = -1;
           }
           else if (y_set >= (int) pLTables->pProject->FrameSizeY)
           {
              y_set = -1;
           }
        }

        // Do scrolling
        if ((x_set >= 0) && (y_set >= 0))
        {
             std::cout << " x_set: " << x_set << " y_set: " << y_set << std::endl;
             Scroll(x_set,y_set);
        }
    }
    
}

void LTLightTable_C::OnPaint( wxPaintEvent &WXUNUSED(event) )
{
    wxPaintDC dc( this );
    PrepareDC( dc );

    DrawShapes(dc);
}


void LTLightTable_C::DoUpdate()
// =======================================================================================
// LTcanvas - Update to canvas after copy or move
// Among other things called from another canvas after copy
// =======================================================================================
{
  // Insert images on display
  bool found;
  for (LightTableImage_C* shapeImage: pLTables->LightTableImages)                      // Run trough LightTableImages_C in LightTable_C to setup shapes to display
  {
      // if image exist do not display
      found = false;
      for (DragShape* TShape: GetDisplayList())
      {
          if (TShape->GetID() == shapeImage->ImageID)
          {
              found = true;
              break;
          }
      }

      if (!found)                           // Image not displaied
      {
          //DragShape* newShape = new DragShape(&shapeImage->thumpnail, shapeImage->ImageID);
          DragShape* newShape = new DragShape(shapeImage);
          newShape->SetPosition(wxPoint(shapeImage->posX,shapeImage->posY));
          newShape->SetDragMethod(SHAPE_DRAG_BITMAP);
          GetDisplayList().push_back(newShape);                               // Create list of shapes in LTLightTable_C
      }
  }
  Refresh(true);                                                              // redraw immediately
}

void LTLightTable_C::OnEraseBackground(wxEraseEvent& event)
{
    if (wxGetApp().GetBackgroundBitmap().IsOk())
    {
        wxSize sz = GetClientSize();
        wxRect rect(0, 0, sz.x, sz.y);

        if (event.GetDC())
        {
            wxGetApp().TileBitmap(rect, *(event.GetDC()), wxGetApp().GetBackgroundBitmap());
        }
        else
        {
            wxClientDC dc(this);
            wxGetApp().TileBitmap(rect, dc, wxGetApp().GetBackgroundBitmap());
        }
    }
    else
        event.Skip(); // The official way of doing it
}

wxPoint LTLightTable_C::VirtuelPosition(const wxPoint& wp)
// =======================================================================================
// Return realpoint from visible point
// =======================================================================================
{
  int x, y; 
  GetViewStart(&x,&y);
  wxPoint realPoint = wp;
  //std::cout << "DEBUG virtuel mouse posx:" << x << " posy:"<< y << "posx" << realPoint.x << " posy:"<< realPoint.y <<"\n";
  realPoint.x += x;
  realPoint.y += y;
  return realPoint; 
}

void LTLightTable_C::OnKeyEventDown(wxKeyEvent& event)
// =======================================================================================
// LTcanvas - Key event (key down)
// =======================================================================================
{
   // shift / control / Alt ....
   Keyboard_Modifiers = event.GetModifiers();

   wxChar uc = event.GetUnicodeKey();
   std::cout << "uc " << uc << std::endl;
   if ( uc != WXK_NONE )
   {
       // It's a "normal" character. Notice that this includes
       // control characters in 1..31 range, e.g. WXK_RETURN or
       // WXK_BACK, so check for them explicitly.
       if ( uc >= 32 )
       {
           if ((uc == 86) && (Keyboard_Modifiers == wxMOD_CONTROL))              // ctrl v
           {
             StartMyProgram();
           }
       }
       else
       {
           // It's a control character
           //wxLogMessage("You pressed control karakter   '%c'", uc);
       }
   }
   else
   {
      unsigned int uc = event.GetKeyCode();
      std::cout << "uc " << uc << std::endl;
      switch (uc)
      {
           case WXK_F5: 
                  DoCopy();
                  break;
 
           case WXK_F6:
                  DoMove();
                  break;
 
           case WXK_PAGEUP:
           case WXK_NUMPAD_PAGEUP:
                  DoScroll(-1,true);
                  break;
 
           case WXK_PAGEDOWN:
           case WXK_NUMPAD_PAGEDOWN:
                  DoScroll(1,true);
                  break;
 
           case WXK_HOME:
                  DoScroll(-2,true);
                  break;
 
           case WXK_END:
                  DoScroll(2,true);
                  break;
 
           case WXK_UP:
                  DoScroll(-3);
                  break;
 
           case WXK_DOWN:
                  DoScroll(3);
                  break;
 
           case WXK_RIGHT:
                  DoScroll(3,false,true);
                  break;
 
           case WXK_LEFT:
                  DoScroll(-3,false,true);
                  break;
 
      }
   }
   
   //std::cout << debugCNT << " noget er trykket " << LTcanvas_ID << "  " << shiftPressed << std::endl;
   //debugCNT++;
   std::cout << "ucyy " << std::endl;
   event.Skip();
}

void LTLightTable_C::OnKeyEventUp(wxKeyEvent& event)
// =======================================================================================
// LTcanvas - Key event (key Up)
// =======================================================================================
{
   // shift / control / Alt ....
   Keyboard_Modifiers = event.GetModifiers();
   //std::cout << debugCNT << " noget er trykket " << LTcanvas_ID << "  " << shiftPressed << std::endl;
   //debugCNT++;
   event.Skip();
}

void LTLightTable_C::OnMouseEvent(wxMouseEvent& event)
{
    if (!pLTFrame->IsActive())    // secure peogram is in focus, before mouse events is done
    {
        return;
    }

    SetFocus();
    //int wheelRotation = event.GetWheelRotation();
    //std::cout << "Mouse er trykket " << LTcanvas_ID << " Wheel " << wheelRotation << std::endl;
    if (event.LeftDown())
    {
        wxPoint vPosition = VirtuelPosition(event.GetPosition());
        DragShape* shape = FindShape(vPosition);
        
        if (shape)
        {
            // We tentatively start dragging, but wait for
            // mouse movement before dragging properly.

            m_dragMode = TEST_DRAG_START;
            m_dragStartPos = VirtuelPosition(event.GetPosition());
            StartPos = shape->GetPosition();
            m_draggedShape = shape;
            if ((Keyboard_Modifiers & wxMOD_SHIFT ) == 0)               // No shift
            {
               pLTables->SelectAll(false);
               pLTables->Select(m_draggedShape->GetID(), true);
            }
            else
            {
               pLTables->Select(m_draggedShape->GetID(), true);
            }
            m_draggedShape->SetShow(true);
            Refresh(true);                   // redraw immediately
           // Update();
        }
        else                                 // No image selected
        {
            if ((Keyboard_Modifiers & wxMOD_SHIFT ) == 0)               // No shift
            {
               pLTables->SelectAll(false);
               Refresh(true);                   // redraw immediately
            }
        }
    }
    //else if (event.LeftUp() && m_dragMode == TEST_DRAG_NONE)
    //{
    //    wxPoint LeftUp_position = VirtuelPosition(event.GetPosition());
    //    std::cout << "DEBUG left up0 Canvas ID:" << LTcanvas_ID << " mouse posx:" << LeftUp_position.x << " posy:"<< LeftUp_position.y <<"\n";
    //}
    else if (event.LeftUp() && m_dragMode != TEST_DRAG_NONE)
    {
        wxPoint LeftUp_position = VirtuelPosition(event.GetPosition());
        //std::cout << "DEBUG left up1 Canvas ID:" << LTcanvas_ID << " mouse posx:" << LeftUp_position.x << " posy:"<< LeftUp_position.y <<"\n";

        // Finish dragging only drag if indside canvas

        m_dragMode = TEST_DRAG_NONE;

        if (!m_draggedShape || !m_dragImage)
            return;

        if ((LeftUp_position.x > 0) && (LeftUp_position.y > 0) && (LeftUp_position.x < sizeCanvas.x) && (LeftUp_position.y < sizeCanvas.y))
        {
            wxPoint newpos = m_draggedShape->GetPosition() + LeftUp_position - m_dragStartPos;
            m_draggedShape->SetPosition(newpos);
            pLTables->UpdateImagePosition(m_draggedShape->GetID(),newpos.x, newpos.y);
        }
        else
        {
            m_draggedShape->SetPosition(StartPos);
        }

        m_dragImage->Hide();
        m_dragImage->EndDrag();
        wxDELETE(m_dragImage);

        m_draggedShape->SetShow(true);

        m_currentlyHighlighted = (DragShape*) NULL;

        m_draggedShape = (DragShape*) NULL;

        Refresh(true);
        Update();
    }
    else if (event.RightDown())
    {
        wxPoint lPosition = event.GetPosition();
        wxPoint vPosition = VirtuelPosition(lPosition);
        DragShape* shape = FindShape(vPosition);
        
        if (shape)
        {
            // Make popup menu for moving
            wxMenu im_menu;
            im_menu.Append( Event_copy, wxT("copy F5"), wxT("copy selected images"));
            im_menu.Append( Event_move, wxT("move F6"), wxT("move selected images"));
            PopupMenu(&im_menu, lPosition);
        }
    }
    else if (event.GetWheelRotation() > 0)
    {
                   DoScroll(-15);
    }
    else if (event.GetWheelRotation() < 0)
    {
                   DoScroll(15);
    }
    else if (event.Dragging() && m_dragMode != TEST_DRAG_NONE)
    {
        if (m_dragMode == TEST_DRAG_START)
        {
            // We will start dragging if we've moved beyond a couple of pixels
            //std::cout << "TEST_DRAG_START\n";
            int tolerance = 2;
            int dx = abs(VirtuelPosition(event.GetPosition()).x - m_dragStartPos.x);
            int dy = abs(VirtuelPosition(event.GetPosition()).y - m_dragStartPos.y);
            if (dx <= tolerance && dy <= tolerance)
                return;

            // Start the drag.
            m_dragMode = TEST_DRAG_DRAGGING;

            if (m_dragImage)
                delete m_dragImage;

            // Erase the dragged shape from the canvas
            m_draggedShape->SetShow(false);

            // redraw immediately
            Refresh(true);
            Update();

            switch (m_draggedShape->GetDragMethod())
            {
                case SHAPE_DRAG_BITMAP:
                {
                    m_dragImage = new MyDragImage(this, *m_draggedShape->GetBitmap(), wxCursor(wxCURSOR_HAND));
                    break;
                }
                case SHAPE_DRAG_TEXT:
                {
                    m_dragImage = new MyDragImage(this, wxString(wxT("Dragging some test text")), wxCursor(wxCURSOR_HAND));
                    break;
                }
                case SHAPE_DRAG_ICON:
                {
                    m_dragImage = new MyDragImage(this, wxICON(dragicon), wxCursor(wxCURSOR_HAND));
                    break;
                }
            }

            bool fullScreen = wxGetApp().GetUseScreen();

            // The offset between the top-left of the shape image and the current shape position
            wxPoint beginDragHotSpot = m_dragStartPos - m_draggedShape->GetPosition();

            // Now we do this inside the implementation: always assume
            // coordinates relative to the capture window (client coordinates)

            //if (fullScreen)
            //    beginDragHotSpot -= ClientToScreen(wxPoint(0, 0));

            if (!m_dragImage->BeginDrag(beginDragHotSpot, this, fullScreen))
            {
                wxDELETE(m_dragImage);
                m_dragMode = TEST_DRAG_NONE;

            } else
            {
                m_dragImage->Move(VirtuelPosition(event.GetPosition()));
                m_dragImage->Show();
            }
        }
        else if (m_dragMode == TEST_DRAG_DRAGGING)
        {
            //std::cout << "TEST_DRAG_DRAGGING\n";
            // We're currently dragging. See if we're over another shape.
            DragShape* onShape = FindShape(VirtuelPosition(event.GetPosition()));

            bool mustUnhighlightOld = false;
            bool mustHighlightNew = false;

            if (m_currentlyHighlighted)
            {
                if ((onShape == (DragShape*) NULL) || (m_currentlyHighlighted != onShape))
                    mustUnhighlightOld = true;
            }

            if (onShape && (onShape != m_currentlyHighlighted) && onShape->IsShown())
                mustHighlightNew = true;

            if (mustUnhighlightOld || mustHighlightNew)
                m_dragImage->Hide();

            // Now with the drag image switched off, we can change the window contents.
            if (mustUnhighlightOld)
                m_currentlyHighlighted = (DragShape*) NULL;

            if (mustHighlightNew)
                m_currentlyHighlighted = onShape;

            if (mustUnhighlightOld || mustHighlightNew)
            {
                Refresh(mustUnhighlightOld);
                Update();
            }

            // Move and show the image again
            m_dragImage->Move(event.GetPosition());

            if (mustUnhighlightOld || mustHighlightNew)
                 m_dragImage->Show();
        }
    }
}

void LTLightTable_C::DrawShapes(wxDC& dc)
{
    //std::cout << "------------------" << std::endl;
    // Get position and size of visible window
    int x_min;
    int y_min;
    GetViewStart(&x_min,&y_min);
    int x_max;
    int y_max;
    GetClientSize(&x_max,&y_max);
    x_max += x_min;
    y_max += y_min;

    for (DragShape* DS : m_displayList)
    {
        // std::cout << "posY/posY_RB " << DS->LightTableImage->posY << "/" << DS->LightTableImage->posY_RB << " y_min/y_max " << y_min << "/" << y_max << std::endl;
        DragShape* shape = DS;
        if (
            (((int) DS->LightTableImage->posY >= y_min) && ((int) DS->LightTableImage->posY<= y_max)) || 
            (((int) DS->LightTableImage->posY_RB >= y_min) &&  ((int) DS->LightTableImage->posY_RB <= y_max))
            )
        {
            //std::cout << "hmnihoem " << shape->GetID() << "  " << shape->IsShown() << "   " << (m_draggedShape != shape) << std::endl;
            //yyyif (shape->IsShown() && m_draggedShape != shape)
            if (shape->IsShown())
            {
                shape->Draw(dc, (m_currentlyHighlighted == shape));
                //std::cout << "Drawing " << shape->GetID() << std::endl; 
            }
        }
    }
}

void LTLightTable_C::EraseShape(DragShape* shape, wxDC& dc)
{
    wxSize sz = GetClientSize();
    wxRect rect(0, 0, sz.x, sz.y);

    wxRect rect2(shape->GetRect());
    dc.SetClippingRegion(rect2.x, rect2.y, rect2.width, rect2.height);

    wxGetApp().TileBitmap(rect, dc, wxGetApp().GetBackgroundBitmap());

    dc.DestroyClippingRegion();
}

void LTLightTable_C::ClearShapes()
{
    for (DragShape* DS : m_displayList)
    {
        DragShape* shape = DS;
        delete shape;
    }
    m_displayList.clear();                         // Clear list with shapes
}

void LTLightTable_C::InsertImage(unsigned int imageID)
// =======================================================================================
// Insert image into canvas
// ---------------------------------------------------------------------------------------
// 
// =======================================================================================
{
   LightTableImage_C* shapeImage = pLTables->InsertImage(imageID,0,0);
   if (shapeImage != NULL)             // Image inserted in LightTable_C => insert in LTCancas too
   {
       //DragShape* newShape = new DragShape(&shapeImage->thumpnail, shapeImage->ImageID);
       DragShape* newShape = new DragShape(shapeImage);
       newShape->SetPosition(wxPoint(shapeImage->posX,shapeImage->posY));
       newShape->SetDragMethod(SHAPE_DRAG_BITMAP);
       //frame->GetCanvas()->GetDisplayList().Append(newShape);
       GetDisplayList().push_back(newShape);                                    // Create list of shapes in LTLightTable_C
   }
}

DragShape* LTLightTable_C::FindShape(const wxPoint& pt) const
{
    for (DragShape* DS : m_displayList)
    {
        DragShape* shape = DS;
        if (shape->HitTest(pt))
            return shape;
    }
    return (DragShape*) NULL;
}

void LTLightTable_C::StartMyProgram()
// =======================================================================================
// This is fast and dirty hack to start nomacs
// The possibility to start other programs should be in a setup 
// system that has not yet been made ....
// =======================================================================================
{
    wxString path = "";
    // Find selectec image
    for (LightTableImage_C* Image: pLTables->LightTableImages)                      // Run trough LightTableImages_C in LightTable_C to setup shapes to display
    {
        if (Image->GetSelect())
        {
            image_C* srcImage = pLTables->pProject->FindImage(Image->ImageID);
            path = wxString::FromUTF8(srcImage->filepath.string());
            break;
        }
    }

    if (path != "")
    {
#ifdef __WXMSW__
      wxString cmd = "c:\\Program Files (x86)\\nomacs\\nomacs-x64\\nomacs.exe "+path;
#else
      wxString cmd = "/usr/bin/nomacs "+path;
#endif
      wxExecute(cmd);
    }
}

//----------------------------------------------------------------------
// DragShape class
//----------------------------------------------------------------------
//DragShape::DragShape(wxBitmap** bitmap, unsigned int i_id)
DragShape::DragShape(LightTableImage_C* i_LightTableImage)
{
    LightTableImage = i_LightTableImage;
    //yyym_bitmap = bitmap;
    m_bitmap = &LightTableImage->thumpnail;
    m_pos.x = 0;
    m_pos.y = 0;
    m_dragMethod = SHAPE_DRAG_BITMAP;
    m_show = true;
    //yyy id = i_id;                          // Id for image_C
}

bool DragShape::HitTest(const wxPoint& pt) const
{
    wxRect rect(GetRect());
    return rect.Contains(pt.x, pt.y);
}

bool DragShape::Draw(wxDC& dc, bool highlight)
{
    if ((*m_bitmap)->IsOk())
    {
        //if (id == 0)
        //{
        //  std::cout << "id " << id << " Bitmap pointer naar der tegnes " << *m_bitmap << "\n";
        //}
        wxMemoryDC memDC;
        memDC.SelectObject(**m_bitmap);
        if (LightTableImage->GetSelect())
        {
            memDC.SetTextForeground(wxColour(255,0,0));
        }
        else
        {
            memDC.SetTextForeground(wxColour(0,0,0));
        }
        memDC.DrawText( wxString::FromUTF8(LightTableImage->FileName),1,LightTableImage->posY_forText);
        

        dc.Blit(m_pos.x, m_pos.y, (*m_bitmap)->GetWidth(), (*m_bitmap)->GetHeight(),
            & memDC, 0, 0, wxCOPY, true);

        if (highlight)
        {
            dc.SetPen(*wxWHITE_PEN);
            dc.SetBrush(*wxTRANSPARENT_BRUSH);
            dc.DrawRectangle(m_pos.x, m_pos.y, (*m_bitmap)->GetWidth(), (*m_bitmap)->GetHeight());
        }

        return true;
    }
    else
        return false;
}

// MyDragImage

// On some platforms, notably Mac OS X with Core Graphics, we can't blit from
// a window, so we need to draw the background explicitly.
bool MyDragImage::UpdateBackingFromWindow(wxDC& WXUNUSED(windowDC), wxMemoryDC& destDC, const wxRect& WXUNUSED(sourceRect),
                    const wxRect& destRect) const
{
    destDC.SetClippingRegion(destRect);

    if (wxGetApp().GetBackgroundBitmap().IsOk())
        wxGetApp().TileBitmap(destRect, destDC, wxGetApp().GetBackgroundBitmap());

    m_canvas->DrawShapes(destDC);
    return true;
}


//-----------------------------------------------------------------------------
// SimLT
//-----------------------------------------------------------------------------
IMPLEMENT_APP(SimLT)

wxBEGIN_EVENT_TABLE(SimLT, wxApp)
//    EVT_MENU(TEST_USE_SCREEN, SimLT::OnUseScreen)
wxEND_EVENT_TABLE()

SimLT::SimLT()
{
    // Drag across whole screen
    m_useScreen = false;
}

bool SimLT::OnInit()
{
    if ( !wxApp::OnInit() )
        return false;

    wxInitAllImageHandlers();

    // Make white background
    wxImage image(150,150);
    image.Clear(255);      
    m_background = wxBitmap(image);

    LTFrame_C* frame = new LTFrame_C();

    frame->Show( true );

    return true;
}

int SimLT::OnExit()
{
    return 0;
}

bool SimLT::TileBitmap(const wxRect& rect, wxDC& dc, wxBitmap& bitmap)
{
    int w = bitmap.GetWidth();
    int h = bitmap.GetHeight();

    int i, j;
    for (i = rect.x; i < rect.x + rect.width; i += w)
    {
        for (j = rect.y; j < rect.y + rect.height; j+= h)
            dc.DrawBitmap(bitmap, i, j);
    }
    return true;
}

//void SimLT::OnUseScreen(wxCommandEvent& WXUNUSED(event))
//{
//    m_useScreen = !m_useScreen;
//}


// --------------------------------------------------------------------
// Copy move dialog (class)
// --------------------------------------------------------------------
CopyMove_C::CopyMove_C(LTFrame_C* LTf, unsigned int defaultLT, wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style):
    wxDialog(parent, id, title, pos, size, wxDEFAULT_DIALOG_STYLE)
{
    // begin wxGlade: CopyMove_C::CopyMove_C
    SetTitle(wxT("Copy Move"));
    wxBoxSizer* sizer_1 = new wxBoxSizer(wxVERTICAL);
    const wxString combo_box_1_choices[] = {};
    combo_box_1 = new wxComboBox(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0, combo_box_1_choices, wxCB_DROPDOWN);
    sizer_1->Add(combo_box_1, 0, wxALIGN_CENTER_HORIZONTAL, 0);
    wxBoxSizer* sizer_2 = new wxBoxSizer(wxHORIZONTAL);
    sizer_1->Add(sizer_2, 1, wxEXPAND, 0);
    button_OK = new wxButton(this, wxID_OK, wxEmptyString);
    button_OK->SetDefault();
    sizer_2->Add(button_OK, 0, 0, 0);
    button_CANCEL = new wxButton(this, wxID_CANCEL, wxEmptyString);
    sizer_2->Add(button_CANCEL, 0, 0, 0);
    
    SetSizer(sizer_1);
    sizer_1->Fit(this);
    SetAffirmativeId(button_OK->GetId());
    SetEscapeId(button_CANCEL->GetId());

    for (LTLightTable_C* LT: LTf->LTs)
    {
        combo_box_1->Append("LT " + std::to_string(LT->LTcanvas_ID));
    }
    combo_box_1->SetSelection(defaultLT);
    
    Layout();

    CanvasID = -1;                           // No canvas found (yet)
    // end wxGlade
}

CopyMove_C::~CopyMove_C()
{
    // Keep order on memory
    delete combo_box_1;
    //delete sizer_1;
    delete button_OK;
    delete button_CANCEL;
    //delete sizer_2;
}

BEGIN_EVENT_TABLE(CopyMove_C, wxDialog)
    // begin wxGlade: CopyMove_C::event_table
    EVT_BUTTON(wxID_OK, CopyMove_C::OKhit)
    // end wxGlade
END_EVENT_TABLE();


void CopyMove_C::OKhit(wxCommandEvent& event)
// =======================================================================================
// OK Key event.
// =======================================================================================
{
    CanvasID = -1;

    std::string ComboBoxItem = (std::string) combo_box_1->GetValue();        // Get item from combobox
    int numPos = ComboBoxItem.find_last_of(' ') + 1;                         // get position of last word
    CanvasID = stoi(ComboBoxItem.substr(numPos));                            // Set CanvasID from item

    event.Skip();
}


int CopyMove_C::getCanvasID()
// =======================================================================================
// Get CanvasID (-1: no valid id)
// =======================================================================================
{
   return CanvasID;
}




