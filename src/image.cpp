// ***************************************************************************************
// Project:     simlt
// Author:      Claus Boje
// Copyright:   (C) 2022 Claus Boje
// Copyright    (C) 2022 Simple Light Table Developers, see AUTHORS.txt for contributors.
//
// Licence:     GPLv3
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// TODO:
//  Heigth for text under thumpnail is hardcoded to 20 pixel, not smart ....
// ***************************************************************************************
#include "image.h"
//yyy#include <vector>
//yyy#include "CImg.h"

image_C::image_C(std::filesystem::path i_filepath, unsigned int i_width_T, unsigned int i_imageID)
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// Constructor - Initialize image
// ---------------------------------------------------------------------------------------
// https://wiki.wxwidgets.org/WxImage
// https://cpp.hotexamples.com/examples/-/wxBitmap/ConvertToImage/cpp-wxbitmap-converttoimage-method-examples.html
// =======================================================================================
{
   filepath = i_filepath;
   imageID = i_imageID;
   wxImage image;
   // Do file exist, if not do not load it.
   //if (std::filesystem::exists(filepath.string()))
   if (std::filesystem::exists(filepath.string()))
   {
      //std::cout << "filepath: " << filepath << std::endl;
      if (image.LoadFile(wxString::FromUTF8(filepath.string()),wxBITMAP_TYPE_JPEG))
      {
          //std::cout << "file loaded: " << filepath << std::endl;
          // Calculate size
          width = image.GetWidth();
          height = image.GetHeight();
          width_T = i_width_T;                                                                      // Maybee this schould be in a setup
          height_T = (unsigned int) (((float) height/((float) width/ (float) width_T)) + 0.5);      // Calculate high.
      
          // Generate images for display
          // Expand image with 20 pixel for pathname
          image.Rescale(width_T, height_T);
          wxImage imageSelected = image.Copy();                         
          wxSize ns((int) width_T, (int) (height_T+20));
          wxPoint np(0,0);
          image=image.Size(ns,np,255,255,255);    // Expand image with white space in bottom.

          //yyy MakeFilenameImg(image,false);
          us_image = new wxBitmap(image);
          imageSelected.RotateHue(0.3);
          imageSelected = imageSelected.Size(ns,np,255,255,255);    // Expand image with white space in bottom.
          s_image = new wxBitmap(imageSelected);
          height_T_forText = height_T + 1;
          height_T += 20;
          loaded_ok = true;
      }
      else
      {
          loaded_ok = false;
      }
   }
   else
   {
       loaded_ok = false;
   }
}

image_C::~image_C()
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// Destrutor - 
// =======================================================================================
{
    if (loaded_ok)
    {
        delete us_image;
        delete s_image;
    }
}

bool image_C::isOK()
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// return true if image ok
// =======================================================================================
{
    return loaded_ok;
}

std::filesystem::path image_C::filepath_rel(std::filesystem::path rel2path)
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// return relativ generic path to image.
// =======================================================================================
{
    return filepath.lexically_relative(rel2path).generic_string();
}

void image_C::MakeFilenameImg(wxImage& im, bool select)
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// select = false: place text as unselcted color
// Select = true: place text as selected image
// return hign of text in pixel
// =======================================================================================:w
//
{
    return;
//    using namespace cimg_library; 
//
//    unsigned char color[] = { 0,0,0 };                // color black
//    unsigned char colorb[] = { 255,255,255 };         // color white
//    if (select)                                       // Image for select
//    {
//        color[0]=255;                                 // Color red
//    }
//
//    // calculate size of text image
//    CImg<unsigned char> imgtextsize;
//    std::string const s = filepath.filename().string();
//    const char* fn = s.c_str();
//    
//    imgtextsize.draw_text(0,0, "%s" ,color, colorb,1,23, fn);  // Draw with font height 23.
//
//    // create textimages
//    CImg<unsigned char> imgtext(imgtextsize.width(), imgtextsize.height(),1,3);
//    imgtext.draw_text(0,0,"%s",color, colorb,1,23,fn);  // Draw with font height 23.
//    //wxBitmap Wximgtext = (wxBitmap) imgtext; 
//
//    // expand image
//    wxSize nst((int) imgtextsize.width(), (int) imgtextsize.height());
//    wxImage wxImageText(nst, imgtext, true); 
//    im = wxImageText.Copy();
//    return;
//    wxSize ns((int) width_T, (int) (height_T+imgtextsize.height()));
//    wxPoint np(0,0);
//    im.Size(ns,np);    // Expand image with white space in bottom.
//    //im.Size(ns,np,255,0,0);    // Expand image with white space in bottom.
//    im.Paste(wxImageText, 100, 0);
//    im.Paste(wxImageText, 0, height_T);
//    height_T += imgtextsize.height();

    //yyy  DEBUG
    //yyy imgtextsize.save("imgtextsize.jpg"); 
    //yyy imgtext.save("imgtext.jpg"); 
    //yyy imgtextselect.save("imgtextselect.jpg"); 
}
