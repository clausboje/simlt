/////////////////////////////////////////////////////////////////////////////
// Name:        simlt.h
// Purpose:     wxDragImage sample
// Author:      Julian Smart
// Modified by:
// Created:     28/2/2000
// Copyright:   (c) Julian Smart
// Licence:     wxWindows licence
/////////////////////////////////////////////////////////////////////////////

#ifndef _WX_DRAGIMAGSAMPLE_
#define _WX_DRAGIMAGSAMPLE_

#include <wx/aui/aui.h>
// derived classes

class LTFrame_C;
class SimLT;
class LTLightTable_C;
class DragShape;
//
// SimLT

class SimLT: public wxApp
{
public:
    SimLT();
    virtual bool OnInit();
    virtual int OnExit();
    //yyy void FileSave(wxCommandEvent& WXUNUSED(event) );
    //yyy void FileLoad(wxCommandEvent& WXUNUSED(event) );

//// Operations

    // Tile the bitmap
    bool TileBitmap(const wxRect& rect, wxDC& dc, wxBitmap& bitmap);

//// Accessors
    wxBitmap& GetBackgroundBitmap() const { return (wxBitmap&) m_background; }

    bool GetUseScreen() const { return m_useScreen; }
    void SetUseScreen(bool useScreen) { m_useScreen = useScreen; }

    //void OnUseScreen(wxCommandEvent& event);

protected:
    wxBitmap    m_background;
    bool        m_useScreen;

DECLARE_EVENT_TABLE()
};

DECLARE_APP(SimLT)

// LTFrame_C

class LTFrame_C: public wxFrame
{
public:
    LTFrame_C();
    ~LTFrame_C();

    project_C project;                                  // Project desciption/setup

    void OnAbout( wxCommandEvent &event );
    void OnQuit( wxCommandEvent &event );
    void OnClose(wxCloseEvent& event);
    void FileSave(wxCommandEvent& WXUNUSED(event) );
    void FileSaveAs(wxCommandEvent& WXUNUSED(event) );
    void ProjectOpen(wxCommandEvent& WXUNUSED(event) );
    void NewLightTable(wxCommandEvent& WXUNUSED(event) );
    void ExportAll(wxCommandEvent& WXUNUSED(event) );

    //void event_Create_LTcanvas(wxCommandEvent& WXUNUSED(event));
    void Create_LTcanvas(int i_ID = -1);
    void event_Create_LT(wxCommandEvent& WXUNUSED(event));
    //void OnKeyEvent(wxKeyEvent& event);
    void OnPaneClose(wxAuiManagerEvent& evt);
    std::list<LTLightTable_C*> LTs;                         // list of canvases

private:
    int LT_id;                                              // Storage for ID to LTcanvas (next posible ID to use)

    unsigned int ImageID;                               // Storage for image id
    wxAuiManager      m_mgr;

    wxDECLARE_DYNAMIC_CLASS(LTFrame_C);
    wxDECLARE_EVENT_TABLE();
};


#define TEST_USE_SCREEN   100

// LTLightTable_C

// Dragging modes
#define TEST_DRAG_NONE     0
#define TEST_DRAG_START    1
#define TEST_DRAG_DRAGGING 2

class LTLightTable_C: public wxScrolledWindow
{
public:
    LTLightTable_C(unsigned int LT_id, wxSize i_size, wxWindow *parent, wxWindowID, const wxPoint &pos, const wxSize &size, long style );
    ~LTLightTable_C();
    
    LTFrame_C*              pLTFrame;                   // Pointer to LTFrame
    LightTable_C*           pLTables;                   // Light Table in project.

    void OnPaint( wxPaintEvent &event );
    void OnEraseBackground(wxEraseEvent& event);
    void OnKeyEventDown(wxKeyEvent& event);
    void OnKeyEventUp(wxKeyEvent& event);
    void OnMouseEvent(wxMouseEvent& event);

    void DrawShapes(wxDC& dc);
    void EraseShape(DragShape* shape, wxDC& dc);
    void ClearShapes();

    void InsertImage(unsigned int imageID);
    //void DeleteImage(unsigned int imageID) {};
    
    void copy_event(wxCommandEvent& WXUNUSED(event));
    int DoCopy();
    void move_event(wxCommandEvent& WXUNUSED(event));
    void DoMove();
    void DoUpdate();                                    // Update canvas with images (call from another canvas after copy)
    void DoScroll(int step, bool autostep=false, bool vertical = false);

    DragShape* FindShape(const wxPoint& pt) const;
    wxPoint VirtuelPosition(const wxPoint& wp);

    std::list<DragShape*>& GetDisplayList() { return m_displayList; }
    unsigned int  LTcanvas_ID;
    bool          shown;

protected:

private:
    std::list<DragShape*>   m_displayList;              // A list of DragShapes
    int                     m_dragMode;
    DragShape*              m_draggedShape;
    DragShape*              m_currentlyHighlighted;     // The shape that's being highlighted
    wxPoint                 m_dragStartPos;             // Start imouse position in shape
    wxPoint                 StartPos;                   // Start position of shape, not mouse position
    wxDragImage*            m_dragImage;
    wxSize                  sizeCanvas;                 // Real size ov canvas
    unsigned int            default_copy_move_item;     // Default copy/move item   // kkk

    int                     Keyboard_Modifiers;         // Alt, Shift, Ctrl .. etc

    unsigned int            debugCNT;
    
    // Fast and dirty
    void                    StartMyProgram();
    

    wxDECLARE_ABSTRACT_CLASS(LTLightTable_C);
    wxDECLARE_EVENT_TABLE();
};


// Ways to drag a shape

#define SHAPE_DRAG_BITMAP       1
#define SHAPE_DRAG_TEXT         2
#define SHAPE_DRAG_ICON         3

// Shape

class DragShape: public wxObject
{
public:
    //DragShape(wxBitmap** bitmap, unsigned int i_id);
    DragShape(LightTableImage_C* i_LightTableImage);
    ~DragShape(){};

//// Operations

    bool HitTest(const wxPoint& pt) const;
    bool Draw(wxDC& dc, bool highlight = false);

//// Accessors

    wxPoint GetPosition() const { return m_pos; }
    void SetPosition(const wxPoint& pos) { m_pos = pos; }

    wxRect GetRect() const { return wxRect(m_pos.x, m_pos.y, (*m_bitmap)->GetWidth(), (*m_bitmap)->GetHeight()); }

    //wxBitmap& GetBitmap() const { return (wxBitmap&) m_bitmap; }
    wxBitmap* GetBitmap() const { return  *m_bitmap; }
    //void SetBitmap(wxBitmap* bitmap) { *m_bitmap = bitmap; }

    int GetDragMethod() const { return m_dragMethod; }
    void SetDragMethod(int method) { m_dragMethod = method; }

    bool IsShown() const { return m_show; }
    void SetShow(bool show) { m_show = show; }

    // yyyunsigned int GetID() { return id; };            // get id for project
    unsigned int GetID() { return LightTableImage->ImageID; };            // get id for image

    LightTableImage_C* LightTableImage;                 // Pointer to image i project

protected:
    wxPoint         m_pos;
    wxBitmap**      m_bitmap;
    int             m_dragMethod;
    bool            m_show;
    unsigned int    id;             // Image id from image_C
};

// MyDragImage
// A derived class is required since we're overriding UpdateBackingFromWindow,
// for compatibility with Mac OS X (Core Graphics) which does not support blitting
// from a window.

class MyDragImage: public wxDragImage
{
public:
    MyDragImage(LTLightTable_C* canvas): m_canvas(canvas) {}

    MyDragImage(LTLightTable_C* canvas, const wxBitmap& image, const wxCursor& cursor = wxNullCursor):
        wxDragImage(image, cursor), m_canvas(canvas)
    {
    }

    MyDragImage(LTLightTable_C* canvas, const wxIcon& image, const wxCursor& cursor = wxNullCursor):
        wxDragImage(image, cursor), m_canvas(canvas)
    {
    }

    MyDragImage(LTLightTable_C* canvas, const wxString& str, const wxCursor& cursor = wxNullCursor):
        wxDragImage(str, cursor), m_canvas(canvas)
    {
    }

    // On some platforms, notably Mac OS X with Core Graphics, we can't blit from
    // a window, so we need to draw the background explicitly.
    virtual bool UpdateBackingFromWindow(wxDC& windowDC, wxMemoryDC& destDC, const wxRect& sourceRect,
                    const wxRect& destRect) const;

protected:
    LTLightTable_C*   m_canvas;
};


// --------------------------------------------------------------------
// Copy move dialog (class)
// --------------------------------------------------------------------
class CopyMove_C: public wxDialog 
{
public:

    CopyMove_C(LTFrame_C* LTf, unsigned int defaultLT, wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos=wxDefaultPosition,
               const wxSize& size=wxDefaultSize, long style=wxDEFAULT_DIALOG_STYLE);
    ~CopyMove_C();
    //virtual void OKhit(wxCommandEvent& event); 
    void OKhit(wxCommandEvent& event); 
    int getCanvasID();

private:
    int CanvasID;                      // -1: No canvas found (yet)

protected:
    wxComboBox* combo_box_1;
    wxButton* button_OK;
    wxButton* button_CANCEL;

    DECLARE_EVENT_TABLE();

}; 




#endif
    // _WX_DRAGIMAGSAMPLE_
