// ***************************************************************************************
// Project:     simlt
// Author:      Claus Boje
// Copyright:   (C) 2022 Claus Boje
// Copyright    (C) 2022 Simple Light Table Developers, see AUTHORS.txt for contributors.
//
// Licence:     GPLv3
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// ***************************************************************************************
#include <iostream>
#include <fstream>
#include <sstream>
#include <wx/textctrl.h>
#include <filesystem>
#include <set>
#include "project.h"


// ------------------------------------------------------------
// Implementation project_C
// ------------------------------------------------------------

project_C::project_C()
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// Constructor
// =======================================================================================
{
    project_loaded = false;
    image_ID = 0;
    //nol NumberOfLightTables = 0;
    ProjectFilePath.clear();               // no project path
    FrameSizeX = 0;
    FrameSizeY = 0;
    ProjectNotSaved_store = false;
}

project_C::~project_C()
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// Clean up 
// =======================================================================================
{
    // Clean up memory
    for (image_C* im : images)
    {
       delete im;
    }

    for (LightTable_C* cv : LightTables)
    {
       delete cv;
    }
}


bool project_C::ProjectNotSaved()
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// Return project saved status
// =======================================================================================
{
        return ProjectNotSaved_store;
};

void project_C::Set_NotSaved()
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// Set project saved status to save
// =======================================================================================
{
        ProjectNotSaved_store = true;
};

bool project_C::NewProject(std::string i_path)
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// Start new project, typical called from GUI or CLI (not implemented yet  :-(  )
// Not from project file (ltp)
// =======================================================================================
{
         bool retval = false;
         imageThumpnailWidth = 200;                                        // Width of images, hardcoded - maybe it should be calculated
         NumberOfHorisontal = 4;                                           // Number of horisontal images, hardcoded - maybe it should be calculated
         PixelBetween = 20;                                                // Number of pixels between images, hardcoded - maybe it should be calculated
         NumberOfVerticalFree =  4;                                        // Free space (in collums) for moving of imagies            
         NumberOfHorisontalFree = 2;                                       // Free space (in rows) for moving of imagies

                                                                           // Get all images files from i_path
         namespace fs = std::filesystem;
         fs::path lpath = (fs::path) i_path;
         std::set<fs::path> sorted_by_name;
         maxVertical = 0;                                                  // Store vertical maxsize of thumnail
         for (const auto& entry : fs::directory_iterator(lpath))           // Run trough directory
            sorted_by_name.insert(entry.path());


         for (auto& filename : sorted_by_name)
         {
             //std::string filenameStr = entry.path().filename().string();
             // yyy fs::path filenameStr = entry.path().filename();                                 // Only used for debug
             std::string extention = filename.extension().string();
 
             if ((extention == ".jpg") || (extention == ".JPG"))                             // Look for image, only jpg now, shoud be expanded to others (JPG, jpeg, tiff, png .....) 
             {
                 image_C* im = new image_C(filename, imageThumpnailWidth, image_ID);                             // Read image and gi it an ID
                 if (im->isOK())
                 {
                    // DEBUG image
                    //if (maxVertical == 0)
                    //{
                    //    im->MakeFilenameImg();
                    //}
                    // DEBUG image
                    images.push_back(im);                                                        // Put the pointer in images list
                    if (im->height_T > maxVertical)                                              // Store max of vertical size og thumpnail
                    {
                         maxVertical = im->height_T;
                    }
                    image_ID++;
                    project_loaded = true;
                    retval = true;
                 }
                 else
                 {
                    delete im;
                 }
             }
         }

         // Calculate size of LightTable
         FrameSizeX = (NumberOfHorisontal + NumberOfVerticalFree) * (imageThumpnailWidth+PixelBetween) + 2 * PixelBetween;
         FrameSizeY = (((image_ID/NumberOfHorisontal)+2)+NumberOfHorisontalFree)*(maxVertical+PixelBetween) + 2 * PixelBetween;

         // Create new Lighttable
         LightTable_C* LightTable = AddLightTable(0);       // Create first ligthTable 
    
         // Copy alle images to 
         LightTableImage_C* LightTableImage;
         unsigned int HorisontalCNT = 0;
         unsigned int x = PixelBetween;

         // Calculate top space
         unsigned int y;
         if (maxVertical > imageThumpnailWidth)
         {
            y = NumberOfHorisontalFree * (imageThumpnailWidth+PixelBetween) + PixelBetween;
         }
         else
         {
            y = NumberOfHorisontalFree * (maxVertical+PixelBetween) + PixelBetween;
         }

         int local_maxVertical = 0;
         for (image_C* im : images)
         {
            LightTableImage = LightTable->InsertImage(im->GetImageID(),x,y,im);                 // Insert image in LightTable
            if (LightTableImage->thumpnail->GetHeight() > local_maxVertical) 
            {
              local_maxVertical =  LightTableImage->thumpnail->GetHeight();
            }

            HorisontalCNT++;                                                // Calculate next position
            if (HorisontalCNT < NumberOfHorisontal)
            {
               x += imageThumpnailWidth + PixelBetween;
            }
            else
            {
               x = PixelBetween;
               y += local_maxVertical + PixelBetween; 
               local_maxVertical  = 0;
               HorisontalCNT = 0;
            }
         }

         if (retval)
         {
            Set_NotSaved();
         }
         return retval;
}

bool project_C::SaveProject(std::string i_path)
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// i_path, path to ltp file
// Save project
//
// Format af ltp file
// 
// NumberOfLightTable: ###                                                  #Number of LightTables
// Image: ID:###,<path relativ to ltp file>                                            #Path: path to image, ID intenal ID
// 
// LightTableDef: LightTableID:###,sizex:###, sizey:###                         #LightTableID: LightTable ID, posx/y: position in cancas
// LightTableImage: LightTableID:###,ImageID:###,posx:###, posy:###             #LightTableID: LightTable ID, posx/y: position in cancas
// =======================================================================================
{
    std::ofstream p_file;
    p_file.open (i_path);
    if (p_file.is_open())
    {

         ProjectFilePath = i_path;                                      // Store path for project
         std::filesystem::path u_path(i_path);
         p_file << "ProjectName: " << u_path.stem() << "\n";
         ProjectName = u_path.stem().string();

         p_file << "imageThumpnailWidth: " << imageThumpnailWidth << "\n";
         p_file << "NumberOfHorisontal : " << NumberOfHorisontal << "\n";
         p_file << "PixelBetween : " << PixelBetween << "\n";
         p_file << "NumberOfHorisontalFree : " << NumberOfHorisontalFree << "\n";

         //nol p_file << "NumberOfLightTable: " << NumberOfLightTables << "\n";

         // Write all images to ltp file
         for (image_C* im : images)
         {
            p_file << "Image: ID:" << im->GetImageID() << "," << im->filepath_rel(u_path.parent_path()) << "\n";
         }

         // Write all LightTables to prj file
         for (LightTable_C* cv : LightTables)
         {
            p_file << "LightTableDef: LightTableID:" << cv->getLightTable_ID() <<",sizex:" << FrameSizeX  << ",sizey:" << FrameSizeY << "\n";
         }

         // Write all LightTables to prj file
         for (LightTable_C* cv : LightTables)
         {
            for (LightTableImage_C* ci : cv->LightTableImages)
            {
               p_file << "LightTableImage: LightTableID:" << cv->getLightTable_ID() <<",ImageID:" << ci->ImageID << ",posx:" << ci->posX << ",posy:" << ci->posY << "\n";
            }
         }

         p_file.close();
         
         ProjectNotSaved_store = false;                           // Info about saved project
         return true;
    }
    else
    {
       return false;
    }
}

void project_C::ExportAll(std::string i_path)
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// Save all ligth tabels for my database
// This is very specific for my use, here we need somthing everyon can use ;-)
// =======================================================================================
{
    // Remove "_LTx" from path
    unsigned int  pos = i_path.find_last_of("_");
    if (pos < i_path.length())                                   // "_" found
    {
        if (i_path.substr(pos, 3) == "_LT")
        {
            std::string tmp_string = i_path.substr(pos);
            unsigned int length = tmp_string.find_first_of(".");
            if (length < tmp_string.length())                        // we have a "."
            {
                i_path.replace(pos, length, "");                    // replase _LTxx with "empty string"
            }
            else
            {
                i_path = i_path.substr(0,pos);
            }
        }
    }
    //yyystd::cout << "Export path: " << i_path << std::endl;

    namespace fs = std::filesystem;
    fs::path u_path(i_path);

    //yyystd::cout << "Export path: " << u_path << std::endl;
    //yyystd::cout << "Export path ext: " << u_path.extension() << std::endl;
    //yyystd::cout << "Export path notext: " << u_path.stem() << std::endl;

    // Run trough all LightTabeles
    for (LightTable_C* LT : LightTables)
    {
        if (!LT->LightTableImages.empty())
        {
            fs::path LT_path = u_path.parent_path();
            LT_path /= u_path.stem(); 
            LT_path += "_LT" + std::to_string(LT->getLightTable_ID() + 1);
            LT_path += u_path.extension();

            std::cout << "LT path: " << LT_path << std::endl;
            LT->SaveLightTableOutput(LT_path);
        }
    }
}

bool project_C::OpenProject(std::string i_path)
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// Createn new project from project file (ltp)
// i_path, path to ltp file
// Load project
// =======================================================================================
{
    unsigned int pos;
    unsigned int nextpos;
    unsigned int linenb = 1;
    std::ifstream p_file;
    std::string tmp_string;
    std::string tmp_string2;
    unsigned int ID;
    std::string path;
    std::string tmp_debug;

    // Variable for insert of images in LightTabels
    LightTable_C* LTforImage = NULL;
    int LightTableID = -1;
    int AktuLightTableID;
    int posx = -1;
    int posy = -1;


    p_file.open (i_path);
    if (p_file.is_open())
    {
        std::filesystem::path u_path(i_path);
        while (std::getline(p_file, tmp_string))
        {
            //std::cout << "DEBUG Linenr: " << linenb << " "  << tmp_string << std::endl;   // DEBUG

            pos = tmp_string.find_first_of(":");
            if (pos < 1023)                        // : found
            {
               if (tmp_string.substr(0, pos) == "Image")
               {
                   // find ,
                   nextpos = tmp_string.find_first_of(",");
                   ID = stoi(tmp_string.substr(pos+5,nextpos-(pos+5)));
                   std::filesystem::path path = u_path.parent_path();
                   path /= tmp_string.substr(nextpos+2,tmp_string.size() - (nextpos+3));
                   std::cout << "DEBUG opne path " << path << std::endl;

                   // Get image from disk
                   image_C* im = new image_C(path, imageThumpnailWidth, ID);            // Read image and give it an ID
                   if (im->isOK())
                   {
                      images.push_back(im);                                                        // Put the pointer in images list
                      if (im->height_T > maxVertical)                                              // Store max of vertical size og thumpnail
                      {
                         maxVertical = im->height_T;
                      }
                    
                      // Set next image_ID
                      if (ID >= image_ID)
                      {
                         image_ID = ID + 1;
                      }
                   }
                   else
                   {
                      delete im;
                      std::cout << linenb << " Fejl i load af: " << path << std::endl;
                   }
                   //std::cout << linenb << " Image " << ID << " path " << path << std::endl;
               }
               else if (tmp_string.substr(0, pos) == "LightTableDef")     // Make LightTables
               {
                   nextpos = tmp_string.find("LightTableID:") + 13;
                   tmp_string2 = tmp_string.substr(nextpos,tmp_string.size()-nextpos);
                   nextpos = tmp_string2.find_first_of(",");
                   unsigned int c_ID = stoi(tmp_string2.substr(0,nextpos));
                   // Set FrameSizeX/Y in project if not set
                   if ((FrameSizeX == 0) && (FrameSizeX == 0))
                   {
                        nextpos = tmp_string2.find("sizex:") + 6;
                        tmp_string2 = tmp_string2.substr(nextpos,tmp_string.size()-nextpos);
                        nextpos = tmp_string2.find_first_of(",");
                        FrameSizeX = stoi(tmp_string2.substr(0,nextpos));
                        nextpos = tmp_string2.find("sizey:") + 6;
                        tmp_string2 = tmp_string2.substr(nextpos,tmp_string.size()-nextpos);
                        nextpos = tmp_string2.find_first_of(",");
                        FrameSizeY = stoi(tmp_string2.substr(0,nextpos));
                   }
                   AddLightTable(c_ID);                   
                   //std::cout << linenb << " LightTableDef ID: " << c_ID << " FX " << FrameSizeX << " FY " << FrameSizeY << std::endl;
               }
               else if (tmp_string.substr(0, pos) == "LightTableImage")
               {
                   // Find LightTableID
                   nextpos = tmp_string.find("LightTableID:") + 13;
                   tmp_string2 = tmp_string.substr(nextpos,tmp_string.size()-nextpos);
                   nextpos = tmp_string2.find_first_of(",");
                   AktuLightTableID = stoi(tmp_string2.substr(0,nextpos));
                   // Find ImageID
                   nextpos = tmp_string.find("ImageID:") + 8;
                   tmp_string2 = tmp_string.substr(nextpos,tmp_string.size()-nextpos);
                   nextpos = tmp_string2.find_first_of(",");
                   ID = stoi(tmp_string2.substr(0,nextpos));       // ImageId

                   nextpos = tmp_string2.find("posx:") + 5;
                   tmp_string2 = tmp_string2.substr(nextpos,tmp_string.size()-nextpos);
                   nextpos = tmp_string2.find_first_of(",");
                   posx = stoi(tmp_string2.substr(0,nextpos));
                   nextpos = tmp_string2.find("posy:") + 5;
                   tmp_string2 = tmp_string2.substr(nextpos,tmp_string.size()-nextpos);
                   nextpos = tmp_string2.find_first_of(",");
                   posy = stoi(tmp_string2.substr(0,nextpos));

                   // Find LightTable and if new ligthtable set pointer up
                   if (LightTableID != AktuLightTableID)
                   {
                       LightTableID = AktuLightTableID;        // Store ID
                       for (LightTable_C* LT : LightTables)
                       {
                           if ((int) LT->getLightTable_ID() == LightTableID)
                           {
                              LTforImage = LT;
                              break;
                           }
                       }
         
                   }

                   // Insert image
                   LTforImage->InsertImage(ID,posx,posy);

                   //std::cout << linenb << " LightTableID:" << LightTableID << " ID: " << ID << "posx, posy: " << posx << ","<< posy << std::endl;

               }
               else if (tmp_string.substr(0, pos) == "ProjectName")
               {
                   ProjectName = tmp_string.substr(pos+3,tmp_string.size() - (pos+3+1));
                   std::cout << linenb << " ProjectName " << ProjectName << std::endl;
               }
               else if (tmp_string.substr(0, pos) == "imageThumpnailWidth")
               {
                   imageThumpnailWidth = stoi(tmp_string.substr(pos+2,3));
                   std::cout << linenb << " imageThumpnailWidth " << imageThumpnailWidth << std::endl;

               }
               else if (tmp_string.substr(0, pos) == "NumberOfHorisontal")
               {
                   NumberOfHorisontal = stoi(tmp_string.substr(pos+2,3));
                   std::cout << linenb << " NumberOfHorisontal " << NumberOfHorisontal << std::endl;

               }
               else if (tmp_string.substr(0, pos) == "PixelBetween")
               {
                   PixelBetween = stoi(tmp_string.substr(pos+2,3));
                   std::cout << linenb << " PixelBetween " << PixelBetween << std::endl;

               }
               else if (tmp_string.substr(0, pos) == "NumberOfHorisontalFree")
               {
                   NumberOfHorisontalFree = stoi(tmp_string.substr(pos+2,3));
                   std::cout << linenb << " NumberOfHorisontalFree " << NumberOfHorisontalFree << std::endl;

               }
               else if (tmp_string.substr(0, pos) == "NumberOfLightTable")
               {
                   //nol NumberOfLightTables = stoi(tmp_string.substr(pos+2,3));
                   //nol std::cout << linenb << " NumberOfLightTable BRUGES IKEK SKAL SLETTES" << NumberOfLightTables << std::endl;

               }
               linenb++;
            }
        }
        p_file.close();

        project_loaded = true;
        ProjectNotSaved_store = false;                           // Project loaded
        return true;
    }
    else
    {
       return false;
    }
}

LightTable_C* project_C::AddLightTable(unsigned int i_ID)
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// Insert LightTable in project if not already inserted
// Retval pointer to LightTable
// =======================================================================================
{
      for (LightTable_C*  LT : LightTables)
      {
         if ((LT->getLightTable_ID()) == i_ID)
         {
            return LT;         // ID exist do nothing (return pointer til LightTable)
         }
      }

      // Create new LightTable
      LightTable_C* LightTable = new LightTable_C(this, i_ID);

      LightTables.push_back(LightTable);
      return LightTable;
}

std::string &project_C::GetProjectFilePath()                    // Return number of LightTables
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// Rerutn ProjectFilePath
// =======================================================================================
{
   return ProjectFilePath;
}


unsigned int project_C::GetNumberOfLightTables()                    // Return number of LightTables
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// Insert LightTable in project
// =======================================================================================
{
   return LightTables.size();
}

image_C* project_C::FindImage(unsigned int ImageID)
// ------------------------------------------------------------
// Find image from ImageID
// THis could be done smart, I am quite sure :-)
// ------------------------------------------------------------
{
   for (image_C* im : images)
   {
        if (im->GetImageID() == ImageID)
        {
            return im;
        }
   }

   return NULL; 
}


int project_C::CopyToLightTable(unsigned int FromLTID, int ToLightTableID)
// ------------------------------------------------------------
// Copy selected image from imageID to LightTableID
// FromLTID: Copy selected images from this LightTable
// ToLightTableID: Copy to this LightTable
//             -1  find first available LightTable
// Return ID of to LightTable  -1 = no ToLightTable is found
// ------------------------------------------------------------
{
    LightTable_C* fromLightTable = NULL; 
    LightTable_C* toLightTable = NULL; 

    if (ToLightTableID == -1)                   // find first available LightTable
    {
         for (LightTable_C* cv : LightTables)
         {
            if ((cv->getLightTable_ID()) != FromLTID)
            {
                ToLightTableID = cv->getLightTable_ID();
                break;
            }
         }
         if (ToLightTableID == -1)
         {
            return -1;                           // No LightTable found !!!
         }
    }
    
    // Set fromLightTable if not exist
    for (LightTable_C* cv : LightTables)
    {
       if ((cv->getLightTable_ID()) == FromLTID)
       {
           fromLightTable = cv;
       }
       if ((cv->getLightTable_ID()) == (unsigned int) ToLightTableID)
       {
           toLightTable = cv;
       }
       if ((fromLightTable != NULL) && (toLightTable != NULL))
       {
          break;
       }
    }

    if ((fromLightTable == NULL) || (toLightTable == NULL))        // LightTables not found
    {
       return -1;
    }

   // Calculate position for images to copy
   unsigned int HorisontalCNT = 0;
   unsigned int x = PixelBetween;
   // Find biggest y
   unsigned int y = 0;
   for (LightTableImage_C* im : toLightTable->LightTableImages)     // Find biggest y
   {
        if (im->posY > y)
        {
            y = im->posY;
        }
   }
   y += (maxVertical+PixelBetween) + PixelBetween;  // Y where we have free space

   for (LightTableImage_C* im : fromLightTable->LightTableImages)
   {
      if (im->GetSelect())                                                      // Find selected
      {
         if (y > (FrameSizeY - ((maxVertical+PixelBetween) + PixelBetween)))    // Ensure y not too big
         {
            y = FrameSizeY - maxVertical-PixelBetween - PixelBetween;
         }

         if (toLightTable->InsertImage(im->ImageID,x,y) !=NULL)                     // Insert image in LightTable
         {
            HorisontalCNT++;                                                       // Calculate next position
            if (HorisontalCNT < NumberOfHorisontal)
            {
               x += imageThumpnailWidth + PixelBetween;
            }
            else
            {
               x = PixelBetween;
               y += maxVertical + PixelBetween; 
               HorisontalCNT = 0;
            }
         }
      }
   }
   Set_NotSaved();                                        // Project not saved
   return ToLightTableID;
}

// ------------------------------------------------------------
// Implementation Cancas_C
// ------------------------------------------------------------
LightTable_C::LightTable_C(project_C* parrent, unsigned int i_ID)
// =======================================================================================
// parrent: pointer to parrent project_C
// i_ID: id for LightTable
// =======================================================================================
{
     pProject = parrent;
     LightTable_ID = i_ID;
     debugCNT = (i_ID+1)*10000;
}

LightTable_C::~LightTable_C()
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// Clean up memory
// =======================================================================================
{
   for (LightTableImage_C* im : LightTableImages)
   {
      delete im;
   }
}

void LightTable_C::GetFreePos(int& posX, int& posY)
{
   posX = 0;
   posY = 0;
}

LightTableImage_C* LightTable_C::InsertImage(unsigned int ImageID, int posX, int posY, image_C* im)
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// Insert image in LightTable
// i_Image_ID: image id (from project_C)
// i_posX, i_posY: position on LightTable
// im: pointer to im if im != NULL
// =======================================================================================
{
   if (im == NULL)                                         // No pointer to im, get one
   {
       im = pProject->FindImage(ImageID);                  // Call parrent roject to get image
       if (im == NULL)                                     // No pointer to im return false
       {
           return NULL;
       }
   }

   // Calculate posX or PosY if < 0
   if ((posX == -1) || (posY == -1))
   {
       GetFreePos(posX, posY);
   }

   // Test if image exist then drop (we can only have one image with the same ID in the list)
   // Posibility to have the same image more than ones needs some programing
   // std::cout << "Mangler for dublering af images i InsertImage" << std::endl;

   for (LightTableImage_C* im_test: LightTableImages)
   {
      if (im_test->ImageID == ImageID)
      {
          return NULL;
      }
   }

   // Generate image used on Ligt Table
   LightTableImage_C* LightTableImage = new LightTableImage_C();
   LightTableImage->ImageID = ImageID;
   LightTableImage->FileName =  im->filepath.filename().string();
   LightTableImage->unselected_thumpnail = im->us_image;
   LightTableImage->thumpnail = im->us_image;
   LightTableImage->selected_thumpnail = im->s_image;
   LightTableImage->posX = posX;
   LightTableImage->posY = posY;
   LightTableImage->posX_RB = posX + im->width_T;
   LightTableImage->posY_RB = posY + im->height_T;
   LightTableImage->posY_forText = im->height_T_forText;
   LightTableImages.push_back(LightTableImage);
 
   pProject->Set_NotSaved();                                        // Project not saved
   return LightTableImage;
}

void LightTable_C::DeleteImage(unsigned int imageID)
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// Delete image
// =======================================================================================
{
 // Remove all selected.
   LightTableImage_C* ptr;
   for (std::list<LightTableImage_C*>::iterator it = LightTableImages.begin(); it != LightTableImages.end();)
   {
        ptr=*it;
        std::cout  << " indhold i LightTable_C " << ptr->ImageID << std::endl;
        if (ptr->ImageID == imageID)
        {
            delete ptr;
            it = LightTableImages.erase(it);
            pProject->Set_NotSaved();                                        // Project not saved
            break;
        }
        else
        {
            it++;
        }
   }
}

int LightTable_C::FindSelected()
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// Delete image if selected
// =======================================================================================
{
   int retval = -1;

   LightTableImage_C* ptr;
   for (std::list<LightTableImage_C*>::iterator it = LightTableImages.begin(); it != LightTableImages.end();)
   {
        ptr=*it;
        std::cout  << " indhold i LightTable_C " << ptr->ImageID << std::endl;
        if (ptr->GetSelect())
        {
            retval = ptr->ImageID;                  // Set retval to ID
            break;
        }
        else
        {
            it++;
        }
   }
   return retval;
}

void LightTable_C::UpdateImagePosition(unsigned int imageID, unsigned int posX, unsigned int posY)
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// Update position of image.
// image_ID: image id (from project_C)
// i_posX, i_posY: position on LightTable
// =======================================================================================
{
   //std::cout << "DEBUG update image pos " << imageID << " " << posX << " " << posY << "\n";
   for (LightTableImage_C* LTim : LightTableImages)
   {
        if (LTim->ImageID == imageID)
        {
            //std::cout << im->posX << " " << im->posY << "\n";
            image_C* im = pProject->FindImage(imageID);                  // Call parrent roject to get image
            if (im != NULL)
            {
                LTim->posX = posX;
                LTim->posY = posY;
                LTim->posX_RB = posX + im->width_T;
                LTim->posY_RB = posY + im->height_T;
                //std::cout << im->posX << " " << im->posY << "\n";
            }
            break;
        }
   }
   pProject->Set_NotSaved();                                        // Project not saved
}

wxBitmap* LightTable_C::Select(unsigned int imageID, bool i_select)
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// Update select / deselect
// return pointer to image to show after select/unselect
// =======================================================================================
{
   for (LightTableImage_C* im : LightTableImages)
   {
        if (im->ImageID == imageID)
        {
            im->SetSelect(i_select);
            return im->thumpnail;              // Change image
        }
   }
   return (wxBitmap*) NULL;
}

void LightTable_C::SelectAll(bool i_select)
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// Select or Deselect all
// =======================================================================================
{
   //DEBUG std::cout << debugCNT << " SelectAll " << LightTable_ID << " i_select " << i_select << std::endl;
   debugCNT++;
   for (LightTableImage_C* im : LightTableImages)
   {
      im->SetSelect(i_select);
      //DEBUG std::cout << debugCNT << " SelectAll hver image " << LightTable_ID << " i_select " << i_select << std::endl;
   }
   UpdatePI = true;
}

bool compare_pos(LightTableImage_C* first, LightTableImage_C* second)
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// Not a part of the class, use for list.sort
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
{
  if (first->posY < second->posY)
  {
    return true;
  }
  else if   (first->posY == second->posY)
  {
    if   (first->posX < second->posX)
    {
       return true;
    }
    else
    {
       return false;
    }
  }
  else
  {
    return false;
  }
}


void LightTable_C::SaveLightTableOutput(std::filesystem::path i_path)                               // Store data in file with image sorte as place on the LightTable 
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// Store all images from LightTables in sortetd order
// =======================================================================================
{
    if (!LightTableImages.empty())              // Images on LightTable ?
    {
        std::list<LightTableImage_C*> LTIs(LightTableImages);

        LTIs.sort(compare_pos);                      // Sort for saving

        std::ofstream p_file;
        p_file.open (i_path);
        if (p_file.is_open())
        {
            for (LightTableImage_C* LTI : LTIs)
            {
                //std::cout << "Sorted" << LTI->ImageID << " posX: " << LTI->posX << "posY: " << LTI->posY << "\n";
                p_file << LTI->FileName << std::endl;
            }
        }
        p_file.close();
    }
}

// ------------------------------------------------------------
// Implementation LightTableImage_C
// ------------------------------------------------------------

void LightTableImage_C::SetSelect(bool i_select)
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// Image selected or not
// =======================================================================================
{
   //wxTextCtrl* text = new wxTextCtrl();
   //wxStreamToTextRedirector redirect(text);
   // this goes to the text control
   if (ImageID == 0)
   {
       std::cout << "Foer skift " << selected << " pointer " << thumpnail << std::endl;
   }
   selected = i_select;
   if (i_select)
   {
       thumpnail = selected_thumpnail;
   }
   else
   {
       thumpnail = unselected_thumpnail;
   }
   if (ImageID == 0)
   {
       std::cout << "Select " << selected << " pointer " << thumpnail << std::endl;
   }
}

bool LightTableImage_C::GetSelect()
// =======================================================================================                                                                                          // ---------------------------------------------------------------------------------------
// Return select status
// =======================================================================================
{
   return selected;
}
