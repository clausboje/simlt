CXX = $(shell wx-config --cxx)
 
PROGRAM = simlt
 
#OBJECTS = $(PROGRAM).o
#OBJECTS := $(patsubst %.cpp,%.o,$(wildcard *.cpp))
OBJECTS := $(patsubst src/%.cpp,obj/%.o,$(wildcard src/*.cpp))
 
# implementation
 
.SUFFIXES:      .o .cpp
 
#.cpp.o:
obj/%.o: src/%.cpp
	$(CXX) -g -c `wx-config --cxxflags` -fmax-errors=5 -o $@ $<
 
all:    $(PROGRAM)
 
$(PROGRAM):     $(OBJECTS)
	$(CXX) -g -o $(PROGRAM) $(OBJECTS) `wx-config --libs std,aui`
 
clean:
	rm -f obj/*.o $(PROGRAM)

